import 'dart:ui' as ui;

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class FacePainter extends CustomPainter {
  final ui.Image image;
  final List<Face> faces;

  FacePainter(this.image, this.faces);


  @override
  void paint(ui.Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0
      ..color = Colors.yellow;

    final Paint facePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0
      ..color = Colors.red;

    final Paint paintCircle = Paint()..color = Colors.blue;

    //draw selected image First
    canvas.drawImage(image, Offset.zero, Paint());

    for (var i = 0; i < faces.length; i++) {
      print("face.headEulerAngleY:__ ${faces[i].headEulerAngleY}");// Head is rotated to the right rotY degrees
      print("face.headEulerAngleZ:__ ${faces[i].headEulerAngleZ}");// Head is tilted sideways rotZ degrees
      // If classification was enabled with FaceDetectorOptions:
      print("face.smilingProbability:__ ${faces[i].smilingProbability}");

      //draw fetched Rectangle around the face
      canvas.drawRect(faces[i].boundingBox, paint);

      // If landmark detection was enabled with FaceDetectorOptions (mouth, ears,eyes, cheeks, and nose available):
      final FaceLandmark leftEar = faces[i].getLandmark(FaceLandmarkType.leftEar);// get one point in the center of leftEar

      //draw circle point in the center of leftEar
      if (leftEar != null) {
        final Offset leftEarPos = leftEar.position;
        canvas.drawCircle(leftEarPos,5,paintCircle);
      }

      // If contour detection was enabled with FaceDetectorOptions (face,mouth, ears,eyes and nose available):
      final face = faces[i].getContour(FaceContourType.face);// get multiple points around that face

      //draw points and lines around that face
      if(face!=null)
      for (var i = 0; i < face.positionsList.length; i++) {
        canvas.drawCircle(face.positionsList[i],5,paintCircle);
        if(i==face.positionsList.length-1)
          canvas.drawLine(face.positionsList[i], face.positionsList[0], facePaint);//if last point draw line between last point and first point
        else
          canvas.drawLine(face.positionsList[i], face.positionsList[i+1], facePaint);
      }

    }
  }


  @override
  bool shouldRepaint(FacePainter oldDelegate) {
    print("shouldRepaint___________________");
    return image != oldDelegate.image || faces != oldDelegate.faces;
  }
}
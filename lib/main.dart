import 'dart:io';
import 'dart:ui' as ui;

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'face_painter.dart';

void main() async {
  //await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

//____________________________________________Load Image from storage_______________________________________________________//
  File _pickedImage;
  final ImagePicker picker = ImagePicker();
  ui.Image _image;

  _pickImage(ImageSource imageSource) async {
    final pickedImageFile = await picker.getImage(
      source: imageSource,
    );
    setState(() {
      _pickedImage = File(pickedImageFile.path);
    });
  }
//____________________________________________________________________________________________________________________________//



//________________________________________________Image Labeler__________________________________________________________//
  _labelImages() async {
    FirebaseVisionImage myImage = FirebaseVisionImage.fromFile(_pickedImage);
    ImageLabeler labeler = FirebaseVision.instance.imageLabeler();
    List<ImageLabel> _imageLabels = await labeler.processImage(myImage);

    //fetch all Labels in the image like bridge and river
    //fetch a percentage of each thing
    String result = "";
    for (ImageLabel imageLabel in _imageLabels) {
      result = result +
          imageLabel.text +
          ":" +
          imageLabel.confidence.toString() +
          "\n";
    }
    print("ResultLabeler____: ${result}");
  }
//____________________________________________________________________________________________________________________________//


//________________________________________________Barcode Detector__________________________________________________________//
  _barCodeScanner() async {
    FirebaseVisionImage myImage = FirebaseVisionImage.fromFile(_pickedImage);
    BarcodeDetector barcodeDetector = FirebaseVision.instance.barcodeDetector();
    List<Barcode> _barCode = await barcodeDetector.detectInImage(myImage);
    String result = "";
    for (Barcode barcode in _barCode) {
      result = barcode.displayValue;
    }
    print("ResultBarCode____: ${result}");
  }
//____________________________________________________________________________________________________________________________//


//________________________________________________Text Recognizer__________________________________________________________//
  _recogniseText() async {
    FirebaseVisionImage myImage = FirebaseVisionImage.fromFile(_pickedImage);
    TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
    VisionText readText = await recognizeText.processImage(myImage);
    String result = "";
    for (TextBlock block in readText.blocks) {
      for (TextLine line in block.lines) {
        result = result + ' ' + line.text + '\n';
      }
    }
    print("ResultText____: ${result}");
  }
//____________________________________________________________________________________________________________________________//


//________________________________________________Face Detection__________________________________________________________//
  bool isFaceDetected = false;//if isFaceDetected == true will show customPainter to draw rect around face
  List<Face> _facesList = List<Face>();
  _recogniseFace() async {
    //convert file to ui.Image
    // ui.Image _image this is image type for custom painter
    final data = await _pickedImage.readAsBytes();
    _image = await decodeImageFromList(data);

    // fetch all faces in the image
    FirebaseVisionImage myImage = FirebaseVisionImage.fromFile(_pickedImage);
    FaceDetector faceDetector = FirebaseVision.instance.faceDetector(FaceDetectorOptions(enableLandmarks: true,enableClassification: true));
    _facesList = await faceDetector.processImage(myImage);

    setState(() {
      isFaceDetected = true;
    });
  }
//____________________________________________________________________________________________________________________________//




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            color: Colors.orangeAccent.withOpacity(0.3),
            width: MediaQuery.of(context).size.width,
            height: 300,
            child: _pickedImage != null
                ? isFaceDetected?FittedBox(
              child: SizedBox(
                width: _image.width.toDouble(),
                height: _image.height.toDouble(),
                child: CustomPaint(
                  painter: FacePainter(_image, _facesList),
                ),
              ),
            ):Image(
                    image: FileImage(_pickedImage),
                  )
                : Center(
                    child: Text("Please Add Image"),
                  ),
          ),
        ),
      ),
      floatingActionButton: FlatButton.icon(
        onPressed: () {
          showDialog(
              context: context,
              builder: (_) {
                return AlertDialog(
                  title: Text(
                    "Complete your action using..",
                  ),
                  actions: [
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "Cancel",
                      ),
                    ),
                  ],
                  content: Container(
                    height: 120,
                    child: Column(
                      children: [
                        ListTile(
                          leading: Icon(Icons.camera),
                          title: Text(
                            "Camera",
                          ),
                          onTap: () {
                            _pickImage(ImageSource.camera);
                            Navigator.of(context).pop();
                          },
                        ),
                        Divider(
                          height: 1,
                          color: Colors.black,
                        ),
                        ListTile(
                          leading: Icon(Icons.image),
                          title: Text(
                            "Gallery",
                          ),
                          onTap: () async {
                            await _pickImage(ImageSource.gallery);
                            //_labelImages();
                            //_barCodeScanner();
                            //_recogniseText();
                            _recogniseFace();
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    ),
                  ),
                );
              });
        },
        icon: Icon(Icons.add),
        label: Text(
          'Add Image',
        ),
      ),
    );
  }
}
